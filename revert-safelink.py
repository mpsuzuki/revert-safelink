#!/usr/bin/env python2
import re
import sys
import getopt
import uuid
import email
import base64
import quopri
import HTMLParser
import lxml.html
import urllib

optlist, args = getopt.getopt(sys.argv[1:], "", [
  "debug",
  "no-count",
  "fallback",
  "email=",
  "safelinker-domain="
])

opthash = {}
opthash["--safelinker-domain"] = "safelinks.protection.outlook.com"
for tpl in optlist:
  if (len(tpl) > 0):
    opthash[ tpl[0] ] = tpl[1]
  else:
    opthash[ tpl[0] ] = True

def recover_safelink_text(inData):
  if (inData == None):
    return

  outLineArr = []

  for inLine in inData.split("\n"):
    outLine = []
    rest = inLine

    while (rest.find(opthash["--safelinker-domain"])):
      toks = re.split("https:\/\/[^\/]*." + opthash["--safelinker-domain"] + "\/\?url=", rest, 2)
      outLine.append( toks.pop(0) )

      if (len(toks) == 0):
        rest = ""
        break

      rest = toks.pop()
      toks = re.split("&data=02%7C01%7C", rest, 2)

      arg = toks.pop(0)
      outLine.append( urllib.unquote(arg) )

      if (len(toks) == 0):
        rest = ""
        break

      rest = toks.pop()
      toks = re.split("reserved=[0-9]+", rest, 2)

      rest = toks.pop()

    if (rest != None and len(rest) > 0):
      outLine.append( rest )

    outLine = "".join(outLine)

    outLineArr.append(outLine)

  return "\n".join(outLineArr)

def recover_safelink_html(payload):
  html = lxml.html.fromstring(payload)

  for elmA in html.cssselect("a"):
    href = elmA.get("href")
    if (href == None or 0 > href.find(opthash["--safelinker-domain"])):
      continue

    toks = re.split("https:\/\/[^\/]*." + opthash["--safelinker-domain"] + "\/\?url=", href, 2)
    toks = re.split("&data=02%7C01%7C" + urllib.quote( opthash["--email"] ), toks.pop(), 2)
    elmA.attrib["href"] = urllib.unquote(toks.pop(0))

  return lxml.html.tostring(html)

# -----------------

msg = email.message_from_file(sys.stdin)

for part_idx, part in enumerate( msg.walk() ):
  cont_type = part.get_content_type()

  if cont_type not in {"text/plain", "text/html"}:
    continue

  payload_decoded = None
  cont_xfer_enc = part.get("Content-Transfer-Encoding")
  if (cont_xfer_enc == None):
    payload_decoded = part.get_payload()
  elif (cont_xfer_enc == "quoted-printable"):
    payload_decoded = quopri.decodestring(part.get_payload())
  elif (cont_xfer_enc == "base64"):
    payload_decoded = base64.b64decode(part.get_payload())

  if (payload_decoded == None):
    continue

  # print payload_decoded
  try:
    cs = part.get_param("charset")
    if (cs == None):
      continue
    cs = cs.lower()
    payload_decoded_unicode = payload_decoded.decode( cs )
  except UnicodeError:
    if (cs == "iso-2022-jp"):
      print >> sys.stderr , "*** inaccurate charset iso-2022-jp is declared, fallback to iso-2022-jp-3 (output would be utf-8)"
      payload_decoded_unicode = payload_decoded.decode( "iso2022_jp_3" )
    elif (cs == "gb2312"):
      print >> sys.stderr , "*** inaccurate charset gb2312 is declared, fallback to gb18030 (output would be utf-8)"
      payload_decoded_unicode = payload_decoded.decode( "gb18030" )
    part.set_charset("utf-8")
    cs = "utf-8"
  
  if (cont_type == "text/plain"):
    payload_decoded_unicode = recover_safelink_text( payload_decoded_unicode )
  elif (cont_type == "text/html"):
    payload_decoded_unicode = recover_safelink_html( payload_decoded_unicode )

  if (cont_xfer_enc == "quoted-printable"):
    part.set_payload( quopri.encodestring( payload_decoded_unicode.encode(cs) ) )
  elif (cont_xfer_enc == "base64"):
    part.set_payload( base64.b64encode( payload_decoded_unicode.encode(cs) ) )
  else:
    part.set_payload( payload_decoded_unicode.encode(cs) )

print (msg.as_string())
