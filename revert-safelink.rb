#!/usr/bin/env ruby
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"
require "optparse"
require "mail"
require "nokogiri"
require "cgi"

module Hash_UseMissingMethodAsHashKey
  def method_missing(mth, *args)
    if (mth[-1..-1] == "=")
      self[mth[0..-2]] = args[0]
      return self
    end
    if (self.include?(mth))
      return self[mth] 
    end

    hph_key = mth.to_s.gsub("_", "-")
    if (self.include?(hph_key))
      return self[hph_key]
    end

    hph_key = mth.to_s.gsub(/([^A-Z])([A-Z])/, '\1 \2').split(" ").collect{|t| t[0..0].downcase + t[1..-1]}.join("-")
    if (self.include?(hph_key))
      return self[hph_key]
    end

    return nil
  end
end

Opts = ARGV.getopts("", "debug", "no-count", "fallback", "email:", "safelinker-domain:safelinks.protection.outlook.com")
Opts.extend(Hash_UseMissingMethodAsHashKey)
Opts["email-escaped"] = '&amp;data=' + CGI.escape( '02|01|' + Opts.email )
Opts["safelinker-domain-regex"] = Regexp.new('https:\/\/[^\/]*.' + Opts.safelinker_domain + '\/\?url=')

STDERR.puts Opts.inspect if (Opts.debug)

class ClassRevert
  attr_accessor(:count, :fail)
  def initialize
    @count = 0
    @fail = false
  end
end

Revert = ClassRevert.new

def removeSafeLinkFromPlainText(input)
  outLineArr = Array.new

  input.split("\n").each do |inLine|
    outLine = ""
    rest = inLine
    while (rest != nil && rest.include?(Opts.safelinker_domain))
      prefix, rest = rest.split(Opts.safelinker_domain_regex, 2)
      outLine += prefix
      safeLinkInfo, rest = rest.split(/reserved=[0-9]+/, 2)
      outLine += CGI.unescape( safeLinkInfo.split( Opts.email_escaped, 2 ).first )
      Revert.count += 1
    end
    outLine += rest if (rest != nil)
    STDERR.puts [inLine, outLine].inspect if (Opts.debug && inLine != outLine)
    outLineArr.push( outLine )
  end
  return outLineArr.join("\n")
end

def removeSafeLinkFromHtml(input)
  html = Nokogiri::HTML::Document.parse(input)

  html.css("a").select{|elmA| elmA["href"] && elmA["href"].include?(Opts.safelinker_domain)}.each do |elmA|
    toks = elmA["href"].split(Opts.safelinker_domain_regex, 2)
    toks = toks.last.split(Opts.email_escaped, 2)
    elmA["href"] = CGI.unescape(toks.first)
    Revert.count += 1

    toks = elmA.text.split( Opts.safelinker_domain_regex, 2 )
    toks = toks.last.split( Opts.email_escaped, 2)
    elmA.children.each{|c| c.remove}
    elmA << Nokogiri::XML::Text.new( CGI.unescape(toks.first), html )
  end

  return html.to_html
end

class Mail::Part
  def revert_safelink
    if (self.multipart?)
      outPart = Mail::Part.new
      outPart.header = self.header.to_s
      self.parts.each_with_index do |aPart, i|
        outPart.add_part( aPart.revert_safelink )
      end
    else
      case (self.content_type.downcase)
      when /text\/plain/ then
        outPart = Mail::Part.new( removeSafeLinkFromPlainText( self.body.decoded ) )
        outPart.header = self.header.to_s
        Revert.count += 1
      when /text\/html/ then
        outPart = Mail::Part.new( removeSafeLinkFromHtml( self.body.decoded ) )
        outPart.header = self.header.to_s
        Revert.count += 1
      else
        outPart = self
      end
    end
    return outPart
  end
end

def makeRevertedMessage(txt)
  inMail = Mail.new(txt)
  if (inMail.multipart?)
    outMail = Mail.new
    outMail.header = inMail.header.to_s
    inMail.parts.each do |aPart|
      outMail.add_part( aPart.revert_safelink )
    end
  else
    outMail = Mail.new( removeSafeLinkFromPlainText( inMail.body.decoded ) )
    outMail.header = inMail.header.to_s
  end
  return outMail
end


origText = STDIN.read
if (Opts.fallback)
  begin
    outMail = makeRevertedMessage(origText)
  rescue => err
    Revert.fail = err
  end
else
  outMail = makeRevertedMessage(origText)
end

if (Revert.count && !Revert.fail)
  puts outMail.to_s
else
  puts origText
end
if (Opts.no_count)
  Revert.count = 0
end
exit Revert.count
