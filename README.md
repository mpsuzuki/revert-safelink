revert-safelink
===============

Usage
-----

```
./revert-safelink.rb \
	--email mpsuzuki@hiroshima-u.ac.jp \
	--safelinker-domain safelinks.protection.outlook.com \
	< [rewritten-message] > [reverted-message]
```

Dependency
----------

https://github.com/mikel/mail
	maybe "ruby-mail" package would serve

Background
----------

in some big organization, sometimes an administrator tries
to rewrite all URLs in all mail messages for hooking by
a proxy - sometimes it's justified as "to protect the
buttheaded members carelessly clicking the nice URLs in the
mail messages", or sometimes it's justified as "to prevent
the disclosure of our nice confidential info".

however, as a side effect of such nice decision, the URLs
in the mail messages become too hard to understand.

so, I wrote these scripts to revert the "safelink"-ed URLs
in mail message.
